# Print out calculated questions in XML Moodle format from a csv file.

**Disclaimer**

This is a very simple script I just wrote quickly in a sunny afternoon because I
needed it. There are no real tests, no setup.py, no installation. Just download
it, install [lxml] and use it. I use it with Python 3.7 (though it should
work with Python 3.6 as well, as I tried on the example file and did not notice
any problem). You should have [lxml] available in your path as it is a
dependency, I did not even try to fall back to `xml.etree.ElementTree` in case
lxml cannot be imported. The csv file is expected to use tabs as field
separators, but modifying this should be straighforward. Again, I did not even
bother :) I might improve all this one day if I have time but this is clearly
not a priority.

[lxml]: http://lxml.de/

Create the questions file in a spreadsheet, one question per sheet, see the
`actualisation.xls` file in the `example` dir. The example is in French as I
wanted to test the script with non-ascii chars, and I removed most of the values
I generated initially (I had 200 sets of values). The answer should be in the
last column. After creating the random values with a formula, using the various
randomize functions available,  those should be copied as values to avoid
regenarating them any time the file is opened. This is especially important for
synced questions (like the ones in the example).

Then export as text or csv or whatever your spreadsheet calls it with the
following parameters:

* Separator: Tab,
* Quoting: Never,
* Character encoding: Unicode (UTF-8),
* locale: United States/English (C).

Finally, run:

    $ python csv_to_moodle_xml.py yourfile.csv

to generate yourfile.xml with default params.

    $ python csv_to_moodle_xml.py --help

for options and parameters.

Then, import the xml file in Moodle, **test** your questions, and enjoy.

